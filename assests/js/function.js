// Выделение Checkbox по клику на Lable
$(document).ready(function () {
    $("input").parent('.products__filter-item-param > label').click(function () {
        var c = $(this).children("input");
        if (c.prop('checked') === true) {
            c.prop('checked', false);
        } else {
            c.prop('checked', true);
        }
    });
});
// Разбиение текста на колонки (autocolumnlist)
$(document).ready(function () {
    $(function () {
        $('.autocolumn').autocolumnlist({
            columns: 2,
            classname: 'col',
            min: 2
        });
    });
});

