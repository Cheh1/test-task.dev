'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const pug = require('gulp-pug');
const browserSync = require('browser-sync').create();
const rename = require('gulp-rename');
const gulpIf = require('gulp-if');
const csso = require('gulp-csso');
const concat = require('gulp-concat');
const compress = require('gulp-uglify');
const imagemin = require('gulp-imagemin');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';
// to production run (не включает sourcemap в конечный файл при сборке): NODE_ENV=production gulp 
const reload = browserSync.reload;

// Compile sass files to css
gulp.task('sass', function () {
  return gulp.src('./assests/css/**/*.scss')
      .pipe(gulpIf(isDevelopment, sourcemaps.init()))
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browser: ['last 2 versions', 'IE 9'],
        cascade: false
      }))
      .pipe(csso())
      .pipe(gulpIf(isDevelopment, sourcemaps.write()))
      .pipe(rename("all.min.css"))
      .pipe(gulp.dest('./site/css'))
      .pipe(browserSync.reload({stream:true}))
});

// Compile pug files to html
gulp.task('pug', () =>{
  return gulp.src('_pugfiles/**/*.pug')
    .pipe(pug(
//    {pretty: true}
    ))
    .pipe(gulp.dest('./_include', [' ']))
    .pipe(gulp.dest('./site'))
});

//Concatenates files
gulp.task('scripts', function() {
  gulp.src('./assests/js/**/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('site/js'));
});

//Compress JS
gulp.task('compress', function () {
  gulp.src("./site/js/all.js")
  .pipe(compress())
  .pipe(rename("all.min.js"))
  .pipe(gulp.dest("./site/js"));
});

//Minify PNG, JPEG, GIF and SVG images
gulp.task('imagemin', () =>
    gulp.src('./img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('site/img'))
);

// the working directory
gulp.task('browser-sync', [
    'sass', 
    'pug', 
    'scripts', 
    'compress', 
    'imagemin'
] ,function() {
    browserSync.init({
        server: {
            baseDir: "./site"
        }
    });
});

// Watch files comiling
gulp.task('watch', function () {
  gulp.watch('./assests/css/*.scss', ['sass']);
  gulp.watch('./_pugfiles/**/*.pug', ['pug']);
  gulp.watch('./site/*.html').on('change', reload);
  gulp.watch('./assests/js/*.js', ['js']);
  gulp.watch('./site/js/*.js').on('change', reload)
});


gulp.task('default', ['watch', 'browser-sync']);
